package exam7mo;

public class Foto {
	private String[] personasEnFoto;
	private String nombreArchivo;
	private int tamanio;
	private String descripcion;
	
	
	public Foto(String nombreArchivo) {
		super();
		this.nombreArchivo = nombreArchivo;
	}
	
	
	public String[] getPersonasEnFoto(int pos) {
		return personasEnFoto;
	}
	
	
	public void setPersonasEnFoto(int pos, String[] personasEnFoto) {
		this.personasEnFoto = personasEnFoto;
	}
	
	
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	
	
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	public int getTamanio() {
		return tamanio;
	}
	
	
	public void setTamanio(int tamanio) {
		this.tamanio = tamanio;
	}
	
	
	public String getDescripcion() {
		return descripcion;
	}
	
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	

}
