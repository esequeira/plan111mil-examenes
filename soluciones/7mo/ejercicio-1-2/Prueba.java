package exam7mo;

public class Prueba {

	public static void main(String[] args) {
		Foto foto1 = new Foto("Img1.jpg");
		foto1.setTamanio(1024);
		Foto foto2 = new Foto("Img2.jpg");
		foto2.setTamanio(1024);
		Foto foto3 = new Foto("Img3.jpg");
		foto3.setTamanio(0);
		
		Album album1 = new Album("Brasil-2018", 10);
		album1.addFoto(foto1);
		album1.addFoto(foto2);
		album1.addFoto(foto3);
		
		
		System.out.println("El archivo se llama: "+album1.getFotos(2).getNombreArchivo());
		System.out.println("El tamanio del archivo es: "+album1.getFotos(2).getTamanio()+ " bytes");
		System.out.println(album1.tieneFotoConTamanioMenor(0));
		System.out.println(album1.tieneFotoConTamanioMenor(1024));

		
		

	}

}
