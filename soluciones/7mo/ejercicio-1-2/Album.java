package exam7mo;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;

public class Album {

	private List<Foto> fotos;
	private String nombre;
	private int cantidadMaxima;
	private Date fechaCreacion;
	
	
	public Album(String nombre, int cantidadMaxima) {
		super();
		this.nombre = nombre;
		this.cantidadMaxima = cantidadMaxima;
		this.fotos = new ArrayList<Foto>();
		this.fechaCreacion = new Date();
	}
	public Foto getFotos(int pos) {
		
		int cantidad = fotos.size();
		System.out.println("La posicion ingresada es: " +pos);
		if(pos > cantidad) {
			System.out.println("El indice no existe");
		}else if(cantidad < 0) {
			return null;
		}else {
			return fotos.get(pos);
		}
		return null;
	}
	public void setFotos(List<Foto> fotos) {
		this.fotos = fotos;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getCantidadMaxima() {
		return cantidadMaxima;
	}
	public void setCantidadMaxima(int cantidadMaxima) {
		this.cantidadMaxima = cantidadMaxima;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	
	public boolean addFoto(Foto foto) {
		return fotos.add(foto);
	}
	
	public boolean tieneFotoConTamanioMenor(int umbral) {
		if(umbral == 0) {
			System.out.println("La foto fue almacenada de forma erronea");
			return true;
		}
		System.out.println("La foto fue almacenada de forma correcta");
		return false;
	}
	
	
	
}
