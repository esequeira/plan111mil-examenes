package exam01;

import java.util.ArrayList;
import java.util.List;

public class Revista {

	private List<Articulo> elementos;
	private String titulo;
	private int ejemplar;
	
	public List<Articulo> getElementos() {
		return elementos;
	}
	
	public int getCantidadElementos() {
		return elementos.size();
	}
	
	public Articulo getArticuloEnPosicion(int posicion ) {
		
		int cantidad = elementos.size();
		System.out.println("La posicion ingresada es: " +posicion);
		if(posicion > cantidad) {
			System.out.println("El indice no existe");
		}else if(cantidad < 0) {
			return null;
		}else {
			return elementos.get(posicion);
		}
		return null;
			
	}
	
	public int getCantidadArticulosDeTema(String unTema){
		int contador = 0;
		for (int i = 0; i <= elementos.size() - 1; i++) {
			boolean result = elementos.get(i).getTemas().contains(unTema);
			
            if(result == true) {
            	contador = contador + 1;
            }
        }
		return contador;
	}

	public void setElementos(List<Articulo> elementos) {
		this.elementos = elementos;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getEjemplar() {
		return ejemplar;
	}

	public void setEjemplar(int ejemplar) {
		this.ejemplar = ejemplar;
	}

	public Revista() {
		// TODO Auto-generated constructor stub
	}

	public Revista(String titulo, int ejemplar) {
		super();
		this.titulo = titulo;
		this.ejemplar = ejemplar;
		this.elementos = new ArrayList<Articulo>();
	}
	
	public boolean addElemento(Articulo articulo) {
		return elementos.add(articulo);
	}

}
