package ejer5;
import java.util.ArrayList;
import java.util.List;

public class GestorDePaquetesDeProyectores {

	private List<PaqueteProyectores> paquetes;
	private String nombreEmpresa;
	
	
	public GestorDePaquetesDeProyectores(String nombreEmpresa) {
		super();
		this.nombreEmpresa = nombreEmpresa;
		this.paquetes = new ArrayList<PaqueteProyectores>();
	}
	public String getNombreEmpresa() {
		return nombreEmpresa;
	}
	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}
	
	public void agregarPaquete(PaqueteProyectores unPaqueteNuevo) {
		 this.paquetes.add(unPaqueteNuevo);
	}
	public List<PaqueteProyectores> getPaquetes() {
		return paquetes;
	}
	public void setPaquetes(List<PaqueteProyectores> paquetes) {
		this.paquetes = paquetes;
	}
	
	public PaqueteProyectores buscarPaqueteDeProyectores(int unCodPaquete) {
		for (int i = 0; i <= paquetes.size() - 1; i++) {
			if(paquetes.get(i).getCodPaqueteDeProyectores() == unCodPaquete) {
				PaqueteProyectores p =  paquetes.get(i);
				return p;
			}
		}
		return null;
	}

	
}
