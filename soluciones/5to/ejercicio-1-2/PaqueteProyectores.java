package ejer5;

public class PaqueteProyectores {
	private int codPaqueteDeProyectores;
	private String destinatario;
	private String destino;
	private float costoEnvio;
	private boolean enTransito;
	
	public int getCodPaqueteDeProyectores() {
		return codPaqueteDeProyectores;
	}
	
	
	public PaqueteProyectores(int codPaqueteDeProyectores, String destinatario, String destino, float costoEnvio) {
		super();
		this.codPaqueteDeProyectores = codPaqueteDeProyectores;
		this.destinatario = destinatario;
		this.destino = destino;
		this.costoEnvio = costoEnvio;
		this.enTransito = true;
	}


	public void setCodPaqueteDeProyectores(int codPaqueteDeProyectores) {
		this.codPaqueteDeProyectores = codPaqueteDeProyectores;
	}
	
	public String getDestinatarios() {
		return destinatario;
	}
	
	public void setDestinatarios(String destinatarios) {
		this.destinatario = destinatarios;
	}
	
	public String getDestino() {
		return destino;
	}
	
	public void setDestino(String destino) {
		this.destino = destino;
	}
	
	public float getCostoEnvio() {
		return costoEnvio;
	}
	
	public void setCostoEnvio(float costoEnvio) {
		this.costoEnvio = costoEnvio;
	}
	
	public boolean isEnTransito() {
		return enTransito;
	}
	
	public void setEnTransito(boolean enTransito) {
		this.enTransito = enTransito;
	}
	
	
	
}
