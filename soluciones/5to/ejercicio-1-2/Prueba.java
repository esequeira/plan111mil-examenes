package ejer5;

public class Prueba {

		public static void main(String[] args) {
			
			GestorDePaquetesDeProyectores gp1 = new GestorDePaquetesDeProyectores("AlMundo");
			
			PaqueteProyectores pp1 = new PaqueteProyectores(101, "programa1", "CABA", 50);
			PaqueteProyectores pp2 = new PaqueteProyectores(102, "programa2", "CABA", 60);
			PaqueteProyectores pp3 = new PaqueteProyectores(105, "programa3", "Palermo", 80);
			PaqueteProyectores pp4 = new PaqueteProyectores(110, "programa30", "CABA", 100);
			PaqueteProyectores pp5 = new PaqueteProyectores(150, "programa100", "Retiro", 250);
			PaqueteProyectores pp6 = new PaqueteProyectores(178, "programa1100", "Hurlingham", 350);
			
			gp1.agregarPaquete(pp1);
			gp1.agregarPaquete(pp2);
			gp1.agregarPaquete(pp3);
			gp1.agregarPaquete(pp4);
			gp1.agregarPaquete(pp5);
			gp1.agregarPaquete(pp6);
			
			System.out.println(gp1.getPaquetes());
			
			// Pruebo metodo buscar con el codigo 102, le digo que me traiga el programa (destinatario)
			System.out.println(gp1.buscarPaqueteDeProyectores(102).getDestinatarios());
			
	}
}
	