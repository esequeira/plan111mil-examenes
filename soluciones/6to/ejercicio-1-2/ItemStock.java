package ejer6;

import java.util.Date;

public class ItemStock {

	private Computadora computadora;
	private int cantidad;
	private Date ultimaFechaDeModificacion;
	
	public ItemStock(Computadora c, int cantidad) {
		super();
		this.computadora = c;
		this.cantidad = cantidad;
		this.ultimaFechaDeModificacion = new Date();
	}

	public Date getUltimaFechaDeModificacion() {
		return ultimaFechaDeModificacion;
	}

	public void setUltimaFechaDeModificacion(Date ultimaFechaDeModificacion) {
		this.ultimaFechaDeModificacion = ultimaFechaDeModificacion;
	}

	public Computadora getComputadora() {
		return computadora;
	}

	public void setComputadora(Computadora computadora) {
		this.computadora = computadora;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public boolean add(ItemStock items) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
}
