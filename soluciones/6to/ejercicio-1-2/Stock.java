package ejer6;
import java.util.ArrayList;
import java.util.List;

public class Stock {
	private List<ItemStock> items = new ArrayList<ItemStock>(); 
	

	

	public List<ItemStock> getItems() {
		return items;
	}

	public void setItems(List<ItemStock> newItems) {
		this.items = newItems;
	}
	

	public void addItemStock(ItemStock i) {
		items.add(i);
	}

	// Consultar faltantes
	
	public List<ItemStock> consultarItemsFaltantes(int cantidadMaxima){
		
		List<ItemStock> resultado = new ArrayList<ItemStock>();
		
		for (int i = 0; i <= items.size() - 1; i++) {
			int faltantes = items.get(i).getCantidad();
			
            if(cantidadMaxima > faltantes) {
            	System.out.println("PC: " + items.get(i).getComputadora().getNombre() + " Cantidad: " + items.get(i).getCantidad());
            }
        }
		
		return resultado;
		
	}
	
	public Computadora obtenerComputadoraMasCostosa() {
		Computadora masCostosa = null;
		for(ItemStock item: this.items) {
			if(masCostosa == null ||
					item.getComputadora().getPrecio() >= masCostosa.getPrecio()) 
				masCostosa = item.getComputadora();
		return masCostosa;
		}
		return masCostosa;
		
	}

	public Computadora obtenerComputadoraMasCostosaSolucion() {
		Computadora masCostosa = null;
		for(ItemStock item: this.items) {
			if(masCostosa == null ||
					item.getComputadora().getPrecio() >= masCostosa.getPrecio()) 
				masCostosa = item.getComputadora();
		return masCostosa;
		}
		return masCostosa;
		
	}
	
}
