package ejer6;

public class Prueba {

	public static void main(String[] args) {
		
		// Creo las computadoras
		
		Computadora compu1 = new Computadora();
		compu1.setNombre("Lenovo M700");
		compu1.setPrecio(18000);
		
		Computadora compu2 = new Computadora();
		compu2.setNombre("Lenovo M800");
		compu2.setPrecio(19000);
		
		Computadora compu3 = new Computadora();
		compu3.setNombre("Lenovo M900");
		compu3.setPrecio(35000);
		
		// Creo los items de stock con la cantidad de computadoras
		
		ItemStock its1 = new ItemStock(compu1,30);
		ItemStock its2 = new ItemStock(compu2,20);
		ItemStock its3 = new ItemStock(compu3,15);

		// Creo el item y le agrego los items de stock
		
		Stock s1 =  new Stock();
		s1.addItemStock(its1);
		s1.addItemStock(its2);
		s1.addItemStock(its3);
		
		System.out.println("--Todos los stock--");
		System.out.println(s1.getItems().get(0).getComputadora().getNombre());
		System.out.println(s1.getItems().get(1).getComputadora().getNombre());
		System.out.println(s1.getItems().get(2).getComputadora().getNombre());
		
		System.out.println("--Todos los items con stock menor a 21 (cantidad)--");
		s1.consultarItemsFaltantes(21); // 
		
		// Computadora mas costosa
		
		System.out.println(s1.obtenerComputadoraMasCostosaSolucion().getPrecio());
	}

}
